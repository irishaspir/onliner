package com.onliner.tools.init;

/**
 * Created by iosdeveloper on 07/06/16.
 */
public enum BrowserType {

    REMOTE("firefox*"),
    FIREFOX("firefox"),
    CHROME("");

    private String type;

    BrowserType(String alias) {
        this.type = alias;
    }

    public static BrowserType getTypeByAlias(String alias) {
        for (BrowserType type : BrowserType.values()) {
            if (type.getType().equals(alias.toLowerCase())) {
                return type;
            }
        }
        throw new RuntimeException("No such enum value");
    }

    private String getType() {
        return type;
    }
}
