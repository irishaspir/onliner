package com.onliner.tools.init;


import com.google.common.base.Predicate;
import com.onliner.tools.Config;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.yandex.qatools.allure.annotations.Attachment;


/**
 * Created by iosdeveloper on 02/06/16.
 */
public class BrowserMethods {

    public static Logger logger = LoggerFactory.getLogger(BrowserMethods.class);
    private final int AJAX_TIMEOUT = Config.getAjaxTimeout();
    private final int WAIT_ELEMENT_TIMEOUT = Config.getWaitElementTimeout();
    private final int TIME_OUT_IN_SECONDS = Config.getTimeOutInSeconds();

    private WebDriver driver = null;

    public BrowserMethods() {
        driver = BrowserInit.get().getDriver();
    }


    public void openUrl(String url) {
        logger.trace("Open url: " + url);
        driver.get(url);
    }

    public WebDriver getDriver() {
        if (null != driver) {
            return driver;
        } else {
            throw new IllegalStateException("Driver has not been initialized!");
        }
    }


    @Attachment(type = "image/png")
    protected byte[] takeAllureScreenshot() {
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }

    public void waitElementIsVisible(final WebElement element) {
        logger.trace("Wait" + TIME_OUT_IN_SECONDS + " element " + element + "is visible");
        new WebDriverWait(driver, TIME_OUT_IN_SECONDS).until(ExpectedConditions.visibilityOf(element));
    }

    public void waitForAjaxProcessed() {
        new WebDriverWait(driver, AJAX_TIMEOUT).until(new Predicate<WebDriver>() {
            @Override
            public boolean apply(WebDriver webDriver) {
                return (Boolean) ((JavascriptExecutor) webDriver).executeScript("return jQuery.active == 0");
            }
        });
    }

    public boolean isPresent(By locator) {
        logger.trace("Find element " + locator);
        return driver.findElements(locator).size() > 0;

    }

    public void waitForInvisible(final By locator) {
        logger.trace("Wait " + WAIT_ELEMENT_TIMEOUT + " element " + locator + " is invisible");
        new WebDriverWait(driver, WAIT_ELEMENT_TIMEOUT).until(ExpectedConditions.invisibilityOfElementLocated(locator));
    }


}


