package com.onliner.tools.init;


import com.onliner.tools.Config;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by iosdeveloper on 02/06/16.
 */

public class BrowserInit {

    private static final int PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS = Config.getPageLoadDefaultTimeoutSeconds();
    private static final int COMMAND_DEFAULT_TIMEOUT_SECONDS = Config.getCommandDefaultTimeoutSeconds();

    private static WebDriver driver = null;
    private static Map<Thread, BrowserInit> instances = new HashMap<Thread, BrowserInit>();

    private BrowserInit(WebDriver driver) {
        this.driver = driver;
    }


    public static BrowserInit get() {
        Thread currentThread = Thread.currentThread();
        BrowserInit instance = instances.get(currentThread);
        if (instance != null) {
            return instance;
        }
        instance = createWebDriver();
        instances.put(currentThread, instance);
        return instance;
    }

    private static BrowserInit createWebDriver() {
        BrowserType browserType = Config.getBrowserType();
        WebDriver driver = null;
        switch (browserType) {
            case REMOTE:
                try {
                    BrowserMethods.logger.trace("Open remoute Firefox: PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS" + PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS + "implicitlyWait" + COMMAND_DEFAULT_TIMEOUT_SECONDS);
                    driver = new RemoteWebDriver(new URL("http://" + Config.getLocalHost() + ":" + Config.getPort() + "/wd/hub/"), DesiredCapabilities.firefox());
                } catch (MalformedURLException e) {
                    throw new RuntimeException("Invalid url format");
                }
                break;
            case FIREFOX:
                BrowserMethods.logger.trace("Open Firefox: PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS" + PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS + "implicitlyWait" + COMMAND_DEFAULT_TIMEOUT_SECONDS);
                driver = new FirefoxDriver(getFirefoxProfile());
                break;
        }

        driver.manage().deleteAllCookies();
        driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS,TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(COMMAND_DEFAULT_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        return new BrowserInit(driver);
    }

    private static FirefoxProfile getFirefoxProfile() {
        return new FirefoxProfile();
    }


    public static void killBrowser() {
        Thread currentThread = Thread.currentThread();
        BrowserInit instance = instances.get(currentThread);
        if (instance != null) {
            try {
                instance.driver.close();
                instance.driver.quit();
            } catch (Exception e) {
                throw new RuntimeException("Driver has not been closed!");
            } finally {
                instances.remove(currentThread);
            }
        }
    }

    public WebDriver getDriver() {
        return driver;
    }
}
