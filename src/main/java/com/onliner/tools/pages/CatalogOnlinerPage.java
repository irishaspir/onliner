package com.onliner.tools.pages;


import com.onliner.tools.Config;
import com.onliner.tools.init.BrowserMethods;
import com.onliner.tools.pages.blocks.MainCategoryList;
import com.onliner.tools.pages.blocks.SubcategoryList;

/**
 * Created by iosdeveloper on 02/06/16.
 */
public class CatalogOnlinerPage extends SeleniumPageObject {


    private BrowserMethods browserMethods = new BrowserMethods();
    private MainCategoryList mainCategoriesList = new MainCategoryList();
    private SubcategoryList subcategoryList = new SubcategoryList();


    public CatalogOnlinerPage open() {
        browserMethods.openUrl(new Config().getBaseUrl());
        return this;
    }

    public CatalogOnlinerPage openCategory(String name) {
        mainCategoriesList.getCategoryByName(name).click();
        browserMethods.waitElementIsVisible(mainCategoriesList.getSubcategotyDivByNameCategory(name));
        return this;
    }

    public boolean isSubcategoryListVisible(String name) {
        return mainCategoriesList.getSubcategotyDivByNameCategory(name).isDisplayed();
    }

    public FiltersResultPage openSubcategory(String nameCategory, String name) {
        subcategoryList.getSubcategoryByName(nameCategory,name).click();
        browserMethods.waitForAjaxProcessed();
        return new FiltersResultPage();
    }
}
