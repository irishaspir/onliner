package com.onliner.tools.pages;

import com.onliner.tools.Config;
import com.onliner.tools.init.BrowserMethods;
import com.onliner.tools.pages.blocks.SearchArrow;
import org.openqa.selenium.WebDriver;
import ru.yandex.qatools.htmlelements.loader.HtmlElementLoader;


/**
 * Created by iosdeveloper on 07/06/16.
 */
public class SearchPage {

    public SearchPage(WebDriver driver) {
        HtmlElementLoader.populatePageObject(this, driver);
    }

    private BrowserMethods browserMethods = new BrowserMethods();
    private SearchArrow searchArrow = new SearchArrow();

    public void search(String request) {
        searchArrow.search(request);
    }

}
