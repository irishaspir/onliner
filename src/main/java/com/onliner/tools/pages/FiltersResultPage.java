package com.onliner.tools.pages;

import com.onliner.tools.init.BrowserMethods;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

/**
 * Created by iosdeveloper on 03/06/16.
 */
public class FiltersResultPage extends SeleniumPageObject {

    @FindBy(id = "schema-filter")
    private WebElement filter;

    @FindBy(className = "schema-header__title")
    private WebElement title;

    @FindBy(id = "schema-products")
    private WebElement resultList;

    private BrowserMethods browserMethods = new BrowserMethods();

    public boolean isTitlePresent() {
        return browserMethods.isPresent(By.className("schema-header__title"));
    }

    public boolean isFilterPresent() {
        return browserMethods.isPresent(By.id("schema-filter"));
    }

    public String getTitle() {
        return title.getText();
    }

    public List<WebElement> returnFilterFields() {
        return filter.findElements(By.xpath("//div[contains(@class,'schema-filter')]"));
    }

    public List<WebElement> returnProductList() {
        browserMethods.waitForAjaxProcessed();
        return resultList.findElements(By.className("schema-product__group"));
    }

    public boolean isErrorMessagePresent(){
        browserMethods.waitForAjaxProcessed();
        return resultList.findElements(By.className("schema-products__message")).size() > 0;
    }



}
