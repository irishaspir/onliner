package com.onliner.tools.pages;

import com.onliner.tools.init.BrowserInit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by iosdeveloper on 02/06/16.
 */
abstract public class SeleniumPageObject {

    private WebDriver driver;

    public SeleniumPageObject() {
        this.driver = BrowserInit.get().getDriver();
        PageFactory.initElements(driver, this);
    }
}
