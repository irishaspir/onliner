package com.onliner.tools.pages.blocks;

import com.onliner.tools.init.BrowserMethods;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by ispiridonova on 29.06.2016.
 */
public class MainCategoryList {

    private BrowserMethods browserMethods = new BrowserMethods();

    private List<MainCategory> getMainCategoriesList() {
        List<MainCategory> mainCategoriesList = new ArrayList<MainCategory>();
        List<WebElement> categories = browserMethods.getDriver().findElements(By.className("catalog-navigation-classifier__item"));
        for (WebElement category : categories) {
            mainCategoriesList.add(new MainCategory(category.getText(), category.getAttribute("data-id"), category.getAttribute("href")));
        }
        return mainCategoriesList;
    }

    public WebElement getCategoryByName(String name) {
        List<MainCategory> mainCategoriesList = getMainCategoriesList();
        WebElement mainCategory = null;
        Iterator<MainCategory> itr = mainCategoriesList.iterator();
        while (itr.hasNext()) {
            MainCategory category = itr.next();
            if (name.equals(category.getName())) {
                mainCategory = browserMethods.getDriver().findElement(By.xpath("//li[@data-id = '" + category.getDataId() + "']"));
            }
        }
        return mainCategory;
    }

    public WebElement getSubcategotyDivByNameCategory(String name) {
        List<MainCategory> mainCategoriesList = getMainCategoriesList();
        WebElement subCategoriesDiv = null;
        Iterator<MainCategory> itr = mainCategoriesList.iterator();
        while (itr.hasNext()) {
            MainCategory category = itr.next();
            String str = category.getName();
            if (name.equals(category.getName())) {
                subCategoriesDiv = browserMethods.getDriver().findElement(By.xpath("//div[@data-id = '" + category.getDataId() + "']"));
            }
        }
        return subCategoriesDiv;
    }


}
