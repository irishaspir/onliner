package com.onliner.tools.pages.blocks.filters;

import org.openqa.selenium.WebElement;

/**
 * Created by ispiridonova on 06.07.2016.
 */
public class RangeBlock {

    private WebElement to;
    private WebElement from;

    public RangeBlock(WebElement to, WebElement from) {
        this.to = to;
        this.from = from;
    }

    public void setTextInFrom(String text){
        from.sendKeys(text);
    }

    public void setTextInTo(String text){
        to.sendKeys(text);
    }

}
