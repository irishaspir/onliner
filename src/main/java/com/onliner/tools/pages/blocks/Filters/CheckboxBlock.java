package com.onliner.tools.pages.blocks.filters;

import org.openqa.selenium.WebElement;

/**
 * Created by ispiridonova on 06.07.2016.
 */
public class CheckboxBlock {

    private String name;
    private WebElement checkbox;

    public CheckboxBlock(String name, WebElement checkbox) {
        this.name = name;
        this.checkbox = checkbox;
    }

    public String getName() {
        return name;
    }

    public void setCheckbox() {
        checkbox.click();
    }


}
