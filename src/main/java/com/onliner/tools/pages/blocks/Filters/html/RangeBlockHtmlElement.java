package com.onliner.tools.pages.blocks.filters.html;

import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Block;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import ru.yandex.qatools.htmlelements.element.TextInput;

/**
 * Created by ispiridonova on 08.07.2016.
 */
@Name("Range form")
@FindBy(className = "schema-filter__fieldset")
public class RangeBlockHtmlElement extends HtmlElement {

    @FindBy(xpath = "//input[contains(@data-bind,'value: facet.value.from')]")
    private TextInput from;

    @FindBy(xpath = "//input[contains(@data-bind,'value: facet.value.to')]")
    private TextInput to;

    public void setFrom(String from) {
        this.from.sendKeys(from);
    }

    public void setTo(String to) {
        this.to.sendKeys(to);
    }

}
