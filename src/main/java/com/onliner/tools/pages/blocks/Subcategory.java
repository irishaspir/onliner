package com.onliner.tools.pages.blocks;

/**
 * Created by ispiridonova on 29.06.2016.
 */
public class Subcategory {

    private String title;
    private String href;

    public Subcategory(String name,  String href) {
        this.title = name;
        this.href = href;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

}
