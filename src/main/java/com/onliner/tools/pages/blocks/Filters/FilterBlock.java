package com.onliner.tools.pages.blocks.filters;

import com.onliner.tools.pages.blocks.filters.html.CheckboxBlockHtmlElement;
import com.onliner.tools.pages.blocks.filters.html.RangeBlockHtmlElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementDecorator;
import ru.yandex.qatools.htmlelements.loader.decorator.HtmlElementFactory;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by ispiridonova on 04.07.2016.
 */
public class FilterBlock {

    private WebElement block;
    private WebDriver driver;

    public FilterBlock(WebElement block, WebDriver driver) {
        this.block = block;
        this.driver = driver;
    }

    public String getBlockName() {
        return block.findElement(By.className("schema-filter__label")).getText();
    }

    public void setMore() {
        block.findElement(By.className("schema-filter-control__item")).click();
    }

    public boolean isCheckBoxList() {
        return block.findElements(By.className("schema-filter__list")).size() > 0;
    }

    public boolean isCheckBoxMoreList() {
        return block.findElements(By.className("schema-filter-control schema-filter-control_more")).size() > 0;
    }

    public boolean isRangeFields() {
        return block.findElements(By.className("schema-filter__group")).size() > 0;
    }

    public RangeBlock getRangeBlock() {
        WebElement from = block.findElement(By.xpath("//input[contains(@class,'schema-filter-control__item schema-filter__number-input') and contains(@data-bind,'value: facet.value.from')]"));
        WebElement to = block.findElement(By.xpath("//input[contains(@class,'schema-filter-control__item schema-filter__number-input') and contains(@data-bind,'value: facet.value.to')]"));
        return new RangeBlock(to, from);
    }


    public RangeBlockHtmlElement getRangeBlockHtml() {
        RangeBlockHtmlElement block = HtmlElementFactory.createHtmlElementInstance(RangeBlockHtmlElement.class);
        PageFactory.initElements(new HtmlElementDecorator(driver), block);
        return block;
    }

    public List<CheckboxBlock> getCheckBox() {
        List<WebElement> webElementList = block.findElements(By.className("schema-filter__checkbox-item"));
        List<CheckboxBlock> checkboxBlockList = new ArrayList<CheckboxBlock>();
        for (WebElement list : webElementList) {
            checkboxBlockList.add(new CheckboxBlock(list.findElement(By.className("schema-filter__checkbox-text")).getText(), list.findElement(By.className("i-checkbox__real"))));
        }
        return checkboxBlockList;
    }

    public CheckboxBlockHtmlElement getCheckboxBlockHtml() {
        CheckboxBlockHtmlElement blockHtmlElement = HtmlElementFactory.createHtmlElementInstance(CheckboxBlockHtmlElement.class);
        PageFactory.initElements(new HtmlElementDecorator(driver), blockHtmlElement);
        return blockHtmlElement;
    }


    public List<CheckboxBlock> getCheckBoxMore() {
        List<WebElement> webElementList = block.findElements(By.className("schema-filter-popover__column-item"));
        List<CheckboxBlock> checkboxBlockList = new ArrayList<CheckboxBlock>();
        for (WebElement list : webElementList) {
            checkboxBlockList.add(new CheckboxBlock(list.findElement(By.className("schema-filter__checkbox-text")).getText(), list.findElement(By.className("i-checkbox__real"))));
        }
        return checkboxBlockList;
    }


}
