package com.onliner.tools.pages.blocks.filters;

import com.onliner.tools.init.BrowserMethods;
import com.onliner.tools.pages.blocks.filters.html.CheckboxBlockHtmlElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ispiridonova on 04.07.2016.
 */

public class Filters {

    private BrowserMethods browserMethods = new BrowserMethods();

    private List<FilterBlock> getAllFilterBlocks() {
        List<WebElement> allBlocks = browserMethods.getDriver().findElements(By.className("schema-filter__fieldset"));
        List<FilterBlock> filterBlocks = new ArrayList<FilterBlock>();
        for (WebElement block : allBlocks) {
            filterBlocks.add(new FilterBlock(block, browserMethods.getDriver()));
        }
        return filterBlocks;
    }

    private FilterBlock getBlockByName(String name) {
        FilterBlock gettingBlock = null;
        List<FilterBlock> allBlocks = getAllFilterBlocks();
        for (FilterBlock block : allBlocks) {
            if (name.equals(block.getBlockName())) {
                gettingBlock = block;
                break;
            }
        }
        return gettingBlock;
    }

    public void setRangeBlock(String nameBlock, String from, String to) {
        FilterBlock block = getBlockByName(nameBlock);
        block.getRangeBlock().setTextInFrom(from);
        block.getRangeBlock().setTextInTo(to);
    }

    public void setRangeBlockHtml(String nameBlock, String from, String to) {
        browserMethods.waitForAjaxProcessed();
        FilterBlock block = getBlockByName(nameBlock);
        block.getRangeBlockHtml().setFrom(from);
        block.getRangeBlockHtml().setTo(to);
    }


    public void setCheckBox(String nameBlock, String... argsName) {
        FilterBlock block = getBlockByName(nameBlock);
        List<CheckboxBlock> checkboxBlockList = block.getCheckBox();
        for (String arg : argsName) {
            for (CheckboxBlock checkboxBlock : checkboxBlockList) {
                if (arg.equals(checkboxBlock.getName())) {
                    checkboxBlock.setCheckbox();
                }
            }
        }
    }

    public void setCheckBoxHtmlElement(String nameBlock, String... argsName) {
        browserMethods.waitForAjaxProcessed();
        FilterBlock block = getBlockByName(nameBlock);
        for (String arg : argsName) {
            block.getCheckboxBlockHtml().setCheckbox(arg);
        }
    }

    public void setCheckBoxMore(String nameBlock, String... argsName) {
        FilterBlock block = getBlockByName(nameBlock);
        block.setMore();
        List<CheckboxBlock> checkboxBlockList = block.getCheckBoxMore();
        for (String arg : argsName) {
            for (CheckboxBlock checkboxBlock : checkboxBlockList) {
                if (arg.equals(checkboxBlock.getName())) {
                    checkboxBlock.setCheckbox();
                }
            }
        }
    }
}
