package com.onliner.tools.pages.blocks.filters.html;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;

import java.util.List;

/**
 * Created by ispiridonova on 12.07.2016.
 */
@Name("Checkbox form")
@FindBy(className = "schema-filter__fieldset")
public class CheckboxBlockHtmlElement extends HtmlElement {

    @FindBy(className = "schema-filter__label")
    private WebElement nameBlock;

    @FindBy(xpath = ".//label[@class = 'schema-filter__checkbox-item']")
    private List<WebElement> checkboxes;

    public void setCheckbox(String name) {
        WebElement checkbox = checkboxes.get(0);
        int i = 1;
        while (!checkbox.getText().equals("")) {
            String t=checkbox.getText();
            if (checkbox.getText().equals(name)) {
                checkbox.click();
            }
            checkbox = checkboxes.get(i);
            i++;
        }
    }
}
