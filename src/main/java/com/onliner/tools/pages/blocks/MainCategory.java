package com.onliner.tools.pages.blocks;

/**
 * Created by ispiridonova on 29.06.2016.
 */
public class MainCategory {

    private String name;
    private String dataId;
    private String href;

    public MainCategory(String name, String dataId, String href) {
        this.name = name;
        this.dataId = dataId;
        this.href = href;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDataId() {
        return dataId;
    }

    public void setDataId(String dataId) {
        this.dataId = dataId;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }


}
