package com.onliner.tools.pages.blocks;

import com.onliner.tools.init.BrowserMethods;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by ispiridonova on 29.06.2016.
 */
public class SubcategoryList {


    private BrowserMethods browserMethods = new BrowserMethods();
    private MainCategoryList mainCategoryList = new MainCategoryList();

    public List<Subcategory> getSubcategoriesList(String nameCategory) {
        List<Subcategory> subcategoryList = new ArrayList<Subcategory>();
        List<WebElement> subcategories = mainCategoryList.getSubcategotyDivByNameCategory(nameCategory).findElements(By.className("catalog-navigation-list__link-inner"));
        for (WebElement subcategory : subcategories) {
            subcategoryList.add(new Subcategory(subcategory.getText(), subcategory.getAttribute("href")));
        }
        return subcategoryList;
    }

    public WebElement getSubcategoryByName(String nameCategory,String name) {
        List<Subcategory> subcategoryList = getSubcategoriesList(nameCategory);
        WebElement gettingSubcategory = null;
        Iterator<Subcategory> itr = subcategoryList.iterator();
        while (itr.hasNext()) {
            Subcategory subcategory = itr.next();
            if (name.equals(subcategory.getTitle())) {
                gettingSubcategory =  browserMethods.getDriver().findElement(By.xpath("//a[@title = '" + subcategory.getTitle() + "']"));
                itr.remove();
            }
        }
        return gettingSubcategory;
    }

}
