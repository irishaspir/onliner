package com.onliner.tools.pages.blocks;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Block;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.HtmlElement;
import ru.yandex.qatools.htmlelements.element.TextInput;

/**
 * Created by iosdeveloper on 07/06/16.
 */
@Name("Search form")
@Block(@FindBy(className = "fast-search__form"))
public class SearchArrow extends HtmlElement {

    @Name("search request input")
    @FindBy(className = "fast-search__input")
    private TextInput requestInput;

    public void search(String request) {
        requestInput.sendKeys(request);
    }


}

