package com.onliner.tools.service;

import com.onliner.tools.pages.blocks.filters.Filters;

/**
 * Created by ispiridonova on 04.07.2016.
 */
public class FilterGuiService {

    private Filters filters = new Filters();


    public void setRange(String name, String from, String to) {
        filters.setRangeBlock(name, from, to);
    }


    public void setRangeHtml(String name, String from, String to){
        filters.setRangeBlockHtml(name, from, to);
    }

    public void setCheckboxHtml(String name, String ... args) {
        filters.setCheckBoxHtmlElement(name, args);
    }

    public void setCheckbox(String name, String ... args) {
        filters.setCheckBox(name, args);
    }

    public void setCheckboxMore(String name, String ... args) {
        filters.setCheckBoxMore(name, args);
    }

    public void setFilter(String name){

    }




}
