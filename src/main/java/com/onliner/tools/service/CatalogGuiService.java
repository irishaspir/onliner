package com.onliner.tools.service;

import com.onliner.tools.pages.CatalogOnlinerPage;

/**
 * Created by iosdeveloper on 03/06/16.
 */
public class CatalogGuiService {

    private String nameCategory;
    private CatalogOnlinerPage catalogPage = new CatalogOnlinerPage();

    public void openCategory(String name) {
        catalogPage.open().openCategory(name);
        nameCategory = name;
    }

    public void openSubcategory(String nameSubcategory) {
        if (catalogPage.isSubcategoryListVisible(nameCategory)) {
            catalogPage.openSubcategory(nameCategory, nameSubcategory);
        } else {
            openCategory(nameCategory);
            catalogPage.openSubcategory(nameCategory, nameSubcategory);
        }
    }

    public void openBaseUrl() {
        catalogPage.open();
    }

}
