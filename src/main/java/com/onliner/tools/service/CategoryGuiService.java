package com.onliner.tools.service;

import com.onliner.tools.pages.FiltersResultPage;

/**
 * Created by iosdeveloper on 03/06/16.
 */
public class CategoryGuiService {

    private FiltersResultPage filtersResultPage = new FiltersResultPage();

    public boolean isCarPageLoad() {
        return (filtersResultPage.isTitlePresent() && filtersResultPage.isFilterPresent());
    }

    public String getTitle() {
        if (isCarPageLoad())
            return filtersResultPage.getTitle();
        else return null;
    }

    public boolean isFilterFields() {
        return (filtersResultPage.returnFilterFields().size() > 0);
    }

    public boolean isProductsExist() {

        return !filtersResultPage.isErrorMessagePresent();
    }

}
