package com.onliner.tools.service;

import com.onliner.tools.init.BrowserMethods;
import com.onliner.tools.pages.SearchPage;

/**
 * Created by iosdeveloper on 07/06/16.
 */
public class SearchGuiService {

    private SearchPage searchPage = new SearchPage(new BrowserMethods().getDriver());

    public void search(String request){
        searchPage.search(request);
    }


}
