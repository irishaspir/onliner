package com.onliner.tools;


import com.onliner.tools.init.BrowserType;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by iosdeveloper on 06/06/16.
 */
public class Config {

    private static final String BASE_URL = "base.url";
    private static final String PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS = "PAGE.LOAD.DEFAULT.TIMEOUT.SECONDS";
    private static final String COMMAND_DEFAULT_TIMEOUT_SECONDS = "COMMAND.DEFAULT.TIMEOUT.SECONDS";
    private static final String AJAX_TIMEOUT = "AJAX.TIMEOUT";
    private static final String WAIT_ELEMENT_TIMEOUT = "WAIT.ELEMENT.TIMEOUT";
    private static final String TIME_OUT_IN_SECONDS = "TIME.OUT.IN.SECONDS";

    private static final String BROWSER_TYPE = "BROWSER.TYPE";
    private static final String LOCALHOST = "LOCALHOST";
    private static final String PORT = "PORT";


    private static FileInputStream file;
    private static Properties property = new Properties();

    public String getBaseUrl() {
        try {
            file = new FileInputStream("./etc/onliner.properties");
            property.load(file);
            return property.getProperty(BASE_URL);
        } catch (IOException e) {
            throw new IllegalArgumentException("Properties file is absent");
        }
    }

    public static BrowserType getBrowserType() {
        String browserAlias;
        try {
            file = new FileInputStream("./etc/onliner.properties");
            property.load(file);
            browserAlias = property.getProperty(BROWSER_TYPE);
        } catch (IOException e) {
            throw new IllegalArgumentException("Properties file is absent");
        }
        return BrowserType.getTypeByAlias(browserAlias);
    }

    public static String getLocalHost() {
        try {
            file = new FileInputStream("./etc/onliner.properties");
            property.load(file);
            return property.getProperty(LOCALHOST);
        } catch (IOException e) {
            throw new IllegalArgumentException("Properties file is absent");
        }
    }

    public static String getPort() {
        try {
            file = new FileInputStream("./etc/onliner.properties");
            property.load(file);
            return property.getProperty(PORT);
        } catch (IOException e) {
            throw new IllegalArgumentException("Properties file is absent");
        }
    }

    public static int getPageLoadDefaultTimeoutSeconds() {
        try {
            file = new FileInputStream("./etc/onliner.properties");
            property.load(file);
            return Integer.parseInt(property.getProperty(PAGE_LOAD_DEFAULT_TIMEOUT_SECONDS));
        } catch (IOException e) {
            throw new IllegalArgumentException("Properties file is absent");
        }
    }

    public static int getCommandDefaultTimeoutSeconds() {
        try {
            file = new FileInputStream("./etc/onliner.properties");
            property.load(file);
            return Integer.parseInt(property.getProperty(COMMAND_DEFAULT_TIMEOUT_SECONDS));
        } catch (IOException e) {
            throw new IllegalArgumentException("Properties file is absent");
        }
    }

    public static int getAjaxTimeout() {
        try {
            file = new FileInputStream("./etc/onliner.properties");
            property.load(file);
            return Integer.parseInt(property.getProperty(AJAX_TIMEOUT));
        } catch (IOException e) {
            throw new IllegalArgumentException("Properties file is absent");
        }
    }

    public static int getWaitElementTimeout() {
        try {
            file = new FileInputStream("./etc/onliner.properties");
            property.load(file);
            return Integer.parseInt(property.getProperty(WAIT_ELEMENT_TIMEOUT));
        } catch (IOException e) {
            throw new IllegalArgumentException("Properties file is absent");
        }
    }

    public static int getTimeOutInSeconds() {
        try {
            file = new FileInputStream("./etc/onliner.properties");
            property.load(file);
            return Integer.parseInt(property.getProperty(TIME_OUT_IN_SECONDS));
        } catch (IOException e) {
            throw new IllegalArgumentException("Properties file is absent");
        }
    }

}
