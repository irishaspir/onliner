package com.onliner.tests;

import com.onliner.tools.init.BrowserInit;
import com.onliner.tools.service.CatalogGuiService;
import com.onliner.tools.service.CategoryGuiService;
import com.onliner.tools.service.FilterGuiService;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.onliner.tools.allure.FeatureList.DESKTOP_FEATURE;
import static com.onliner.tools.allure.StoryList.FILTER;

/**
 * Created by ispiridonova on 08.06.2016.
 */
public class FiltersTests {

    private CatalogGuiService catalog = new CatalogGuiService();
    private CategoryGuiService category = new CategoryGuiService();
    private FilterGuiService filter = new FilterGuiService();

    @Features(DESKTOP_FEATURE)
    @Stories(FILTER)
    @Description("Check range filter")
    @Test
    public void setRangeFilterAndCheckResultHtml() {
        catalog.openCategory("Авто и мото");
        catalog.openSubcategory("Легковые автомобили");
        filter.setRangeHtml("Минимальная цена, новые рубли", "5", "100");
        Assert.assertTrue(category.isProductsExist(), "Products are absent.");
    }

    @Features(DESKTOP_FEATURE)
    @Stories(FILTER)
    @Description("Check range filter")
    @Test
    public void setRangeFilterAndCheckResult() {
        catalog.openCategory("Авто и мото");
        catalog.openSubcategory("Легковые автомобили");
        filter.setRange("Минимальная цена, новые рубли", "5", "100");
        Assert.assertTrue(category.isProductsExist(), "Products are absent.");
    }

    @Features(DESKTOP_FEATURE)
    @Stories(FILTER)
    @Description("Check checkbox filter")
    @Test
    public void setCheckboxFilterAndCheckResultHtmlElement() {
        catalog.openCategory("Авто и мото");
        catalog.openSubcategory("Легковые автомобили");
        filter.setCheckboxHtml("Тип автомобиля", "легковой");
        Assert.assertTrue(category.isProductsExist(), "Products are absent.");
    }


    @Features(DESKTOP_FEATURE)
    @Stories(FILTER)
    @Description("Check checkbox filter")
    @Test
    public void setCheckboxFilterAndCheckResult() {
        catalog.openCategory("Авто и мото");
        catalog.openSubcategory("Легковые автомобили");
        filter.setCheckbox("Тип автомобиля", "легковой");
        Assert.assertTrue(category.isProductsExist(), "Products are absent.");
    }

    @Features(DESKTOP_FEATURE)
    @Stories(FILTER)
    @Description("Check checkbox more filter")
    @Test
    public void setCheckboxMoreFilterAndCheckResult() {
        catalog.openCategory("Авто и мото");
        catalog.openSubcategory("Легковые автомобили");
        filter.setCheckboxMore("Производитель", "BMW");
        Assert.assertTrue(category.isProductsExist(), "Products are absent.");
    }

    @AfterTest
    public void killBrowser() {
        BrowserInit.killBrowser();
    }
}
