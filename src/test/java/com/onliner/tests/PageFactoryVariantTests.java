package com.onliner.tests;


import com.onliner.tools.init.BrowserInit;
import com.onliner.tools.init.BrowserMethods;
import com.onliner.tools.service.CatalogGuiService;
import com.onliner.tools.service.CategoryGuiService;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import static com.onliner.tools.allure.FeatureList.DESKTOP_FEATURE;
import static com.onliner.tools.allure.StoryList.FILTER;


/**
 * Created by iosdeveloper on 02/06/16.
 */
public class PageFactoryVariantTests extends BrowserMethods {

    private CatalogGuiService catalog = new CatalogGuiService();
    private CategoryGuiService category = new CategoryGuiService();

    @Features(DESKTOP_FEATURE)
    @Stories(FILTER)
    @Description("Check filter exists")
    @Test
    public void checkFilterExistsResultButtonExistFilterFieldsExist() {
        catalog.openCategory("Авто и мото");
        catalog.openSubcategory("Легковые автомобили");
        Assert.assertTrue(category.isCarPageLoad(), "Car page doesn't load");
        Assert.assertTrue(category.isFilterFields(), "Filter fields don't load");
    }


    @Features(DESKTOP_FEATURE)
    @Stories(FILTER)
    @Description("Check title exists")
    @Test
    public void checkProductTitleExists() {
        catalog.openCategory("Авто и мото");
        catalog.openSubcategory("Легковые автомобили");
        Assert.assertEquals(category.getTitle(), "Автомобили", "Headline doesn't consist expecting one.");
    }

    @Features(DESKTOP_FEATURE)
    @Stories(FILTER)
    @Description("Check products exist")
    @Test
    public void checkProductListIsNotEmpty() {
        catalog.openCategory("Авто и мото");
        catalog.openSubcategory("Легковые автомобили");
        Assert.assertTrue(category.isProductsExist(), "Products are absent.");
    }

    @AfterTest
    public void killBrowser(){
        BrowserInit.killBrowser();
    }

}
